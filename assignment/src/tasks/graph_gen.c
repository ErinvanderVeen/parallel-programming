#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>

#define MAX_NODES 10000
#define MAX_EDGES 500
#define MAX_WEIGHT 1000

int main(int argc, char* argv[]) {
	uint64_t nodec;
	uint64_t edgec;
	uint16_t weight;
	uint64_t dest;

	struct timeval time;
	gettimeofday(&time,NULL);

	srand((time.tv_sec * 1000) + (time.tv_usec / 1000));

	nodec = (rand() % MAX_NODES) + 1;
	//nodec = MAX_NODES;

	printf("%" PRIu64 "\n\n", nodec);

	for (uint64_t i = 0; i < nodec; i++) {
		edgec = rand() % MAX_EDGES;
		//edgec = MAX_EDGES;
		printf("%" PRIu64 "\n", edgec);

		for (uint64_t j = 0; j < edgec; j++) {
			weight = rand() % MAX_WEIGHT;
			dest = rand() % nodec;

			printf("%" PRIu64 " %" PRIu16 "\n", dest, weight);
		}

		printf("\n");
	}

	return 0;
}
