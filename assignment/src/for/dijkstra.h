#pragma once

#include <inttypes.h>
#include <omp.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

// Every edge contains a destination and a weight
// Obviously, the weight could be changed to a floating point number
typedef struct {
	uint64_t dest;
	uint16_t weight;
} Edge;

// Every node stores the number of edges and a pointer to the edge array
// Structs cannot take variable size arrays (obviously)
typedef struct {
	uint64_t edgec;
	Edge *edges;
	uint64_t prev;
	uint16_t dist;
	bool visited;
} Node;

#include "heap.h"
