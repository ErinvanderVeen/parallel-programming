#include "dijkstra.h"

// Indexed by id
// Contains all nodes in a graph
Node *nodes;
uint64_t nodec;

// Priority Queue
heap_t *queue;

// No two nodes should evaluate the same node simultaneously
omp_lock_t* locks;

// Current distance of shortest path,
// note this is used to kill all irrelevant tasks when a path has been found
uint16_t shortest_found = UINT16_MAX;

void dijkstra(uint64_t start, uint64_t goal) {
	push(queue, 0, start);

	while (!empty(queue)) {
		uint64_t n = pop(queue);

		if (nodes[n].visited)
			continue;

		nodes[n].visited = true;

		// Consider every outgoing edge of the current node
#pragma omp parallel
#pragma omp for
		for (uint64_t i = 0; i < nodes[n].edgec; i++) {
			Edge e = nodes[n].edges[i];

			// Lock taget node
			omp_set_lock(&locks[e.dest]);

			uint16_t dist_n_e = nodes[n].dist + e.weight;

			// If the current route is shorter than the existing one, update it
			// And consider the new node.
			if (dist_n_e < nodes[e.dest].dist) {

				// Update distance and route
				nodes[e.dest].prev = n;
				nodes[e.dest].dist = dist_n_e;

				// Only create a new task if the dist is lower
#pragma omp critical
				push(queue, dist_n_e, e.dest);
			}

			// Lock taget node
			omp_unset_lock(&locks[e.dest]);
		}
	}
}

/*
 * Very inefficient method of getting the distance
 * Only for debug purposes
 */
uint16_t find_dist(uint64_t from, uint64_t to) {
	uint16_t min_weight = UINT16_MAX;

	for (uint64_t i = 0; i < nodes[from].edgec; i++) {
		if (nodes[from].edges[i].dest == to && nodes[from].edges[i].weight < min_weight)
			min_weight =  nodes[from].edges[i].weight;
	}
	return min_weight;
}

int main(int argc, char* argv[]) {

	queue = (heap_t *)calloc(1, sizeof (heap_t));

	// Open file argument
	FILE * fp = fopen(argv[1],"r");

	// Read the number of nodes
	fscanf(fp, "%" SCNu64 , &nodec);

	if (nodec == 0) {
		fprintf(stderr, "Need at least 1 node\n");
		exit(EXIT_FAILURE);
	}

	// Allocate enough room for all nodes
	nodes = malloc(nodec * sizeof(Node));

	// Create space for all locks
	locks = malloc(nodec * sizeof(*locks));

	// For-loop over all nodes
	for (uint64_t i = 0; i < nodec; i++) {
		// Initialize the lock for this node
		omp_init_lock(&locks[i]);

		Node* n = &nodes[i];
		n->prev = UINT16_MAX; // i.e. None
		n->dist = UINT16_MAX;
		n->visited = false;
		// The .size and final reference can be removed, but are not for
		// readability
		// Reads the amount of outgoing edges a node has
		fscanf(fp, "%" SCNu64, &(*n).edgec);

		// Create enough space
		n->edges = malloc((*n).edgec * sizeof(Edge));

		// Read every edge
		// Assumes the edges are sorted on input (only relevant for the serial
		// version of the algorithm, probably)
		for (uint64_t j = 0; j < (*n).edgec; j++) {
			Edge *e = &(*n).edges[j];

			fscanf(fp, "%" SCNu64, &(*e).dest);
			fscanf(fp, "%" SCNu16, &(*e).weight);
		}
	}

	fclose(fp);

	nodes[0].dist = 0;

	dijkstra(0, nodec - 1);

	for (uint64_t i = 0; i < nodec; i++)
		omp_destroy_lock(&locks[i]);

	uint64_t to = nodec - 1;
	uint64_t from = nodes[to].prev;

	if (from == UINT16_MAX) {
		fprintf(stderr, "No path found to end\n");
		exit(EXIT_SUCCESS);
	}

	printf("Total Length: %" PRIu16 "\nPath:\n", nodes[to].dist);
	uint16_t sum = 0;

	while(to != 0) {
		uint16_t weight = find_dist(from, to);
		sum += weight;
		printf("%" PRIu64 " --%" PRIu16 "-> %" PRIu64 "\n", from, weight, to);
		to = from;
		from = nodes[to].prev;
	}

	printf("Total Calculated Length: %" PRIu16 "\n", sum);

	if (sum != nodes[nodec - 1].dist) {
		printf("ERRRRRORRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR\n");
		exit(EXIT_FAILURE);
	}

	return 0;
}
