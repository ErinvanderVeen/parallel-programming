module Main where

import ParallelProgramming.MPI

import Control.Distributed.MPI as MPI
import Data.Binary
import Foreign
import Foreign.C.Types

main :: IO ()
main = vectorScale

vectorScale :: IO ()
vectorScale = do
  MPI.init
  rank <- MPI.commRank MPI.commWorld
  size <- MPI.commSize MPI.commWorld
  case rank of
    0 -> master rank size
    _ -> slave rank size
  MPI.finalize
  where
    master :: Rank -> Rank -> IO ()
    master rank size = do
      -- Read intergers and convert to CInt
      (scalar : numbers) <- map (read :: String -> CDouble) . words <$> getLine
      -- Allocate sending buffer
      let numbersSize = length numbers
      -- Send size to all slaves, to allow them to allocate
      bcastSendInt $ fromIntegral numbersSize
      bcastSendDouble scalar

      -- Assume numbers / size is a whole number, otherwise trucate and lose some
      -- numbers
      let r_size = quot numbersSize (fromInteger $ toInteger size)
      msg <- scatterSendDoubleVector numbers numbersSize r_size

      let p = map (* scalar) msg
      res <- gatherRecvDoubleVector numbersSize p
      putStrLn $ show res

    slave :: Rank -> Rank -> IO ()
    slave rank size = do
      numbersSize <- bcastRecvInt
      scalar <- bcastRecvDouble
      let r_size = quot (toInteger numbersSize) (toInteger size)
      msg <- scatterRecvDoubleVector (fromInteger r_size)
      let p = map (* scalar) msg
      gatherSendDoubleVector p

dotProduct :: IO ()
dotProduct = do
  MPI.init
  rank <- MPI.commRank MPI.commWorld
  size <- MPI.commSize MPI.commWorld
  case rank of
    0 -> master rank size
    _ -> slave rank size
  MPI.finalize
  where
    master :: Rank -> Rank -> IO ()
    master rank size = do
      -- Read intergers and convert to CDouble
      vec1 <- map (read :: String -> CDouble) . words <$> getLine
      vec2 <- map (read :: String -> CDouble) . words <$> getLine
      -- Allocate sending buffer
      let vec_size = length vec1
      -- Send size to all slaves, to allow them to allocate
      bcastSendInt $ fromIntegral vec_size

      -- Assume numbers / size is a whole number, otherwise trucate and lose some
      -- numbers
      let r_size = quot vec_size (fromInteger $ toInteger size)
      block1 <- scatterSendDoubleVector vec1 vec_size r_size
      block2 <- scatterSendDoubleVector vec2 vec_size r_size

      let d = sum $ zipWith (*) block1 block2

      res <- reduceRecvDoubleSum d
      putStrLn $ show res

    slave :: Rank -> Rank -> IO ()
    slave rank size = do
      vec_size <- bcastRecvInt
      let r_size = quot (toInteger vec_size) (toInteger size)
      block1 <- scatterRecvDoubleVector (fromInteger r_size)
      block2 <- scatterRecvDoubleVector (fromInteger r_size)

      let d = sum $ zipWith (*) block1 block2

      reduceSendDoubleSum d
