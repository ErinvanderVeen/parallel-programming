{-# LANGUAGE TypeApplications #-}
module ParallelProgramming.MPI
  where

import Control.Distributed.MPI as MPI
import Data.Binary
import Foreign
import Foreign.C.Types

scatterRecvVector :: Int -> IO [CInt]
scatterRecvVector r_size = do
  r_buf <- mallocForeignPtrArray @CInt r_size
  scatter (nullPtr :: Ptr CInt, 0) (r_buf, r_size) MPI.rootRank MPI.commWorld
  withForeignPtr r_buf $ peekArray r_size

scatterSendVector :: [CInt] -> Int -> Int -> IO [CInt]
scatterSendVector vec s_size r_size = do
  s_buf <- mallocForeignPtrArray @CInt s_size
  -- Store numbers in buffer
  withForeignPtr s_buf $ \ptr -> pokeArray ptr vec
  r_buf <- mallocForeignPtrArray @CInt r_size
  scatter (s_buf, s_size :: Int) (r_buf, r_size :: Int) MPI.rootRank MPI.commWorld
  withForeignPtr r_buf $ peekArray r_size

gatherRecvVector :: Int -> [CInt] -> IO [CInt]
gatherRecvVector tsize vec = do
  s_buf <- mallocForeignPtrArray @CInt (length vec)
  r_buf <- mallocForeignPtrArray @CInt tsize
  withForeignPtr s_buf $ \ptr -> pokeArray ptr vec
  gather (s_buf, length vec) (r_buf, length vec) MPI.rootRank MPI.commWorld
  withForeignPtr r_buf $ peekArray tsize

gatherSendVector :: [CInt] -> IO ()
gatherSendVector vec = do
  s_buf <- mallocForeignPtrArray @CInt (length vec)
  withForeignPtr s_buf $ \ptr -> pokeArray ptr vec
  gather (s_buf, length vec) (nullPtr :: Ptr CInt, 0 :: Int) MPI.rootRank MPI.commWorld

bcastRecvInt :: IO CInt
bcastRecvInt = do
  -- Allocate memory
  s_buf <- mallocForeignPtr @CInt
  -- Receive
  bcast (s_buf, 1 :: Int) MPI.rootRank MPI.commWorld
  -- Dereference
  withForeignPtr s_buf peek

bcastSendInt :: CInt -> IO ()
bcastSendInt ci = do
  -- Allocate memory region
  s_buf <- mallocForeignPtr @CInt
  -- Store CInt
  withForeignPtr s_buf $ \ptr -> poke ptr ci
  -- Broadcast
  bcast (s_buf, 1 :: Int) MPI.rootRank MPI.commWorld

scatterRecvDoubleVector :: Int -> IO [CDouble]
scatterRecvDoubleVector r_size = do
  r_buf <- mallocForeignPtrArray @CDouble r_size
  scatter (nullPtr :: Ptr CDouble, 1) (r_buf, r_size) MPI.rootRank MPI.commWorld
  withForeignPtr r_buf $ peekArray r_size

scatterSendDoubleVector :: [CDouble] -> Int -> Int -> IO [CDouble]
scatterSendDoubleVector vec s_size r_size = do
  s_buf <- mallocForeignPtrArray @CDouble s_size
  -- Store numbers in buffer
  withForeignPtr s_buf $ \ptr -> pokeArray ptr vec
  r_buf <- mallocForeignPtrArray @CDouble r_size
  scatter (s_buf, s_size) (r_buf, r_size) MPI.rootRank MPI.commWorld
  withForeignPtr r_buf $ peekArray r_size

gatherRecvDoubleVector :: Int -> [CDouble] -> IO [CDouble]
gatherRecvDoubleVector tsize vec = do
  s_buf <- mallocForeignPtrArray @CDouble (length vec)
  r_buf <- mallocForeignPtrArray @CDouble tsize
  withForeignPtr s_buf $ \ptr -> pokeArray ptr vec
  gather (s_buf, length vec) (r_buf, length vec) MPI.rootRank MPI.commWorld
  withForeignPtr r_buf $ peekArray tsize

gatherSendDoubleVector :: [CDouble] -> IO ()
gatherSendDoubleVector vec = do
  s_buf <- mallocForeignPtrArray @CDouble (length vec)
  withForeignPtr s_buf $ \ptr -> pokeArray ptr vec
  gather (s_buf, length vec) (nullPtr :: Ptr CDouble, 0 :: Int) MPI.rootRank MPI.commWorld

bcastRecvDouble :: IO CDouble
bcastRecvDouble = do
  -- Allocate memory
  s_buf <- mallocForeignPtr @CDouble
  -- Receive
  bcast (s_buf, 1 :: Int) MPI.rootRank MPI.commWorld
  -- Dereference
  withForeignPtr s_buf peek

bcastSendDouble :: CDouble -> IO ()
bcastSendDouble ci = do
  -- Allocate memory region
  s_buf <- mallocForeignPtr @CDouble
  -- Store CDouble
  withForeignPtr s_buf $ \ptr -> poke ptr ci
  -- Broadcast
  bcast (s_buf, 1 :: Int) MPI.rootRank MPI.commWorld

reduceRecvDoubleSum :: CDouble -> IO CDouble
reduceRecvDoubleSum d = do
  s_buf <- mallocForeignPtr @CDouble
  r_buf <- mallocForeignPtr @CDouble
  withForeignPtr s_buf $ \ptr -> poke ptr d
  reduce (s_buf, 1) (r_buf, 1) opSum MPI.rootRank MPI.commWorld
  withForeignPtr r_buf peek

reduceSendDoubleSum :: CDouble -> IO ()
reduceSendDoubleSum d = do
  s_buf <- mallocForeignPtr @CDouble
  withForeignPtr s_buf $ \ptr -> poke ptr d
  reduce (s_buf, 1) (nullPtr :: Ptr CDouble, 0) opSum MPI.rootRank MPI.commWorld

