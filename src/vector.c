#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>

#define ROOT 0

void vector_scale(int my_id, int num_procs) {
	unsigned short int vector_size;
	unsigned short int block_size;

	double scalar;
	double *vector;
	double *block;

	if (my_id == ROOT) {

		scanf("%hu", &vector_size);
		vector = malloc(vector_size * sizeof(*vector));

		for (unsigned short int i = 0; i < vector_size; i++)
			scanf("%lf", &vector[i]);

		scanf("%lf", &scalar);

		block_size = vector_size / num_procs;
	}

	MPI_Bcast(&block_size, 1, MPI_SHORT, ROOT, MPI_COMM_WORLD);
	block = malloc(block_size * sizeof(*block));

	MPI_Bcast(&scalar, 1, MPI_DOUBLE, ROOT, MPI_COMM_WORLD);

	MPI_Scatter(vector, block_size, MPI_DOUBLE, block, block_size, MPI_DOUBLE, ROOT, MPI_COMM_WORLD);

	for (unsigned short int i = 0; i < block_size; i++)
		block[i] *= scalar;

	MPI_Gather(block, block_size, MPI_DOUBLE, vector, block_size, MPI_DOUBLE, ROOT, MPI_COMM_WORLD);

	if (my_id == ROOT) {
		for (unsigned short int i = 0; i < vector_size; i++)
			printf("%lf ", vector[i]);
	}

	if (my_id == ROOT)
		free(vector);
	free(block);
}

void dot_product(int my_id, int num_procs) {
	unsigned short int vector_size;
	unsigned short int block_size;

	double *vector1;
	double *vector2;
	double *block1;
	double *block2;

	double product = 0;

	if (my_id == ROOT) {

		scanf("%hu", &vector_size);
		vector1 = malloc(vector_size * sizeof(*vector1));
		vector2 = malloc(vector_size * sizeof(*vector2));

		for (unsigned short int i = 0; i < vector_size; i++)
			scanf("%lf", &vector1[i]);

		for (unsigned short int i = 0; i < vector_size; i++)
			scanf("%lf", &vector2[i]);

		block_size = vector_size / num_procs;
	}

	MPI_Bcast(&block_size, 1, MPI_SHORT, ROOT, MPI_COMM_WORLD);
	block1 = malloc(block_size * sizeof(*block1));
	block2 = malloc(block_size * sizeof(*block2));

	MPI_Scatter(vector1, block_size, MPI_DOUBLE, block1, block_size, MPI_DOUBLE, ROOT, MPI_COMM_WORLD);
	MPI_Scatter(vector2, block_size, MPI_DOUBLE, block2, block_size, MPI_DOUBLE, ROOT, MPI_COMM_WORLD);

	for (unsigned short int i = 0; i < block_size; i++)
		product += block1[i] * block2[i];

	if (my_id == ROOT) {
		MPI_Reduce(MPI_IN_PLACE, &product, 1, MPI_DOUBLE, MPI_SUM, ROOT, MPI_COMM_WORLD);
		printf("%lf\n", product);
	} else {
		MPI_Reduce(&product, NULL, 1, MPI_DOUBLE, MPI_SUM, ROOT, MPI_COMM_WORLD);
	}
}

int main(int argc, char *argv[]) {
	MPI_Init(&argc, &argv);

	int my_id;
	MPI_Comm_rank(MPI_COMM_WORLD, &my_id);
	int num_procs;
	MPI_Comm_size(MPI_COMM_WORLD, &num_procs);

	dot_product(my_id, num_procs);

	MPI_Finalize();
}
