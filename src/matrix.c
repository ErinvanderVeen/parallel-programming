#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>

#define ROOT 0

void matrix_vector_multiplication(int my_id, int num_procs) {
	double* matrix;
	double* vector;
	double* block;
	double* vector_block;

	// Rows will remain uninitialized for processes other than ROOT
	unsigned short int rows, columns;
	unsigned short int block_columns;

	if (my_id == ROOT) {
		scanf("%hu", &rows);
		scanf("%hu", &columns);

		// We will store the matrix in column-order
		matrix = malloc(columns * rows * sizeof(*matrix));
		vector = malloc(columns * sizeof(*vector));

		// Read Matrix from stdin
		for (unsigned short int i = 0; i < rows; i++)
			for (unsigned short int j = 0; j < columns; j++)
				scanf("%lf", matrix + i * rows + j);

		// Read Vector from stdin
		for (unsigned short int i = 0; i < columns; i++)
			scanf("%lf", &vector[i]);

		block_columns = columns / num_procs;
	}

	MPI_Bcast(&block_columns, 1, MPI_SHORT, ROOT, MPI_COMM_WORLD);
	MPI_Bcast(&columns, 1, MPI_SHORT, ROOT, MPI_COMM_WORLD);
	MPI_Bcast(&rows, 1, MPI_SHORT, ROOT, MPI_COMM_WORLD);

	// Unfortunately, we need to copy the entire vector
	if (my_id != ROOT)
		vector = malloc(columns * sizeof(*vector));

	block = malloc(block_columns * rows * sizeof(*block));
	vector_block = malloc(block_columns * sizeof(*vector_block));

	unsigned short int partial_vector_size = rows / num_procs;

	// We can use scatter because we used column-order
	MPI_Scatter(matrix, block_columns * rows, MPI_DOUBLE, block, block_columns * rows, MPI_DOUBLE, ROOT, MPI_COMM_WORLD);
	MPI_Scatter(vector, block_columns, MPI_DOUBLE, vector_block, block_columns, MPI_DOUBLE, ROOT, MPI_COMM_WORLD);

	// Cannot initialize variable size object :(
	double temp_result[rows];

	for (unsigned short int i = 0; i < rows; i++) {
		temp_result[i] = 0;

		for (unsigned short int j = 0; j < block_columns; j++) {
			// This will cause a bunch of cache-misses
			temp_result[i] += *(block + j * rows + i) * vector_block[j];
		}
	}

	// Allocate temporary receive buffer
	double vector_recv[rows];

	for (int i = 0; i < num_procs; i++) {
		MPI_Gather(&temp_result[i * partial_vector_size], partial_vector_size, MPI_DOUBLE, vector_recv, partial_vector_size, MPI_DOUBLE, i, MPI_COMM_WORLD);
	}

	for (unsigned short int i = 0; i < partial_vector_size; i++) {
		temp_result[i] = 0;
		for (unsigned short int j = 0; j < num_procs; j++) {
			temp_result[i] += vector_recv[i + j * partial_vector_size];
		}
	}

	MPI_Gather(temp_result, partial_vector_size, MPI_DOUBLE, vector_recv, partial_vector_size, MPI_DOUBLE, ROOT, MPI_COMM_WORLD);

	if (my_id == ROOT) {
		for (unsigned short int i = 0; i < rows; i++)
			printf("%lf ", vector_recv[i]);
		printf("\n");
	}

	if (my_id == ROOT)
		free(matrix);
	free(vector);
	free(block);
}

int main(int argc, char *argv[]) {
	MPI_Init(&argc, &argv);

	int my_id;
	MPI_Comm_rank(MPI_COMM_WORLD, &my_id);
	int num_procs;
	MPI_Comm_size(MPI_COMM_WORLD, &num_procs);

	matrix_vector_multiplication(my_id, num_procs);

	MPI_Finalize();
}
