#include <mpi.h>
#include <stdio.h>

int main(int argc, char *argv[]) {
	MPI_Init(&argc, &argv);
	int num_procs;
	MPI_Comm_size(MPI_COMM_WORLD, &num_procs);
	int my_id;
	MPI_Comm_rank(MPI_COMM_WORLD, &my_id);
	printf("Hello from process %d withing a communicator of %d\n",
			my_id, num_procs);
	MPI_Finalize();
}
